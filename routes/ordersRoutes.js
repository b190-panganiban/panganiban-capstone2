const express = require('express');
const router = express.Router();

// Import Order Controller
const ordersControllers = require('../controllers/ordersControllers');

const auth = require('../auth');

const {verify, verifyAdmin} = auth;

// Routes

// GET
router.get('/getAllOrders', verify, verifyAdmin, ordersControllers.getAllOrders);

// POST

module.exports = router;