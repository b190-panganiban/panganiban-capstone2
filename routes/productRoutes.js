const express = require("express");
const router = express.Router();

// import User Controllers
const productControllers = require("../controllers/productControllers");

// checking if authentic user
const auth = require("../auth");

const {verify, verifyAdmin} = auth;


// Routes

// GET

// Retrieve all Products
// localhost:4000/products/getAllProducts
router.get("/getAllProducts", productControllers.getAllProducts);
// localhost:4000/products/getSingleProduct
router.get("/getSingleProduct/:id", productControllers.getSingleProduct);
// Retrieve all Orders
// localhost:4000/products/getAllOrders/:id
router.get("/getAllOrders/:id", verify, verifyAdmin, productControllers.getAllOrders)
// Retrieve all active products
router.get("/getActiveProducts", productControllers.getActiveProducts)
// Get archive products
router.get("/getArchiveProducts", verify, verifyAdmin, productControllers.getArchiveProducts)



// POST

// Add new Product
// localhost:4000/products/addNewItem
router.post("/addNewProduct", verify, verifyAdmin, productControllers.addNewProduct);


// UPDATE

// Update product details
// localhost:4000/products/updateProductDetails/:id
router.put("/updateProductDetails/:id", verify, verifyAdmin, productControllers.updateProductDetails);


// Archive product
// localhost:4000/products/archiveProduct/:id
router.put("/archive/:id", verify, verifyAdmin, productControllers.archive);

// Activate product
// localhost:4000/products/activate/:id
router.put("/active/:id", verify, verifyAdmin, productControllers.active);





module.exports = router;