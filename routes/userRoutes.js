const express = require("express");
const router = express.Router();

// import User Controllers
const userControllers = require("../controllers/userControllers");

// checking if authentic user
const auth = require("../auth");

const {verify, verifyAdmin} = auth;


// ROUTES

// GET
// login
router.get("/", userControllers.loginUser);

// Retrieve
// localhost:4000/users/getAllUserOrder
router.get("/getAllUserOrder", verify, userControllers.getAllUserOrder)


// POST

// register a user
// localhost:4000/users/
router.post("/", userControllers.registerUser);

// OrderProduct
// localhost:4000/users/orderProduct
router.post("/orderProduct", verify, userControllers.orderProduct)

// UPDATE

// update a user to admin
// localhost:4000/users/updateUserToAdmin/:ID
router.put("/updateUserToAdmin/:id", verify, verifyAdmin, userControllers.updateUserToAdmin)

module.exports = router;