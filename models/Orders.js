const mongoose = require("mongoose");

const ordersSchema = new mongoose.Schema({
	userId: {
		type: String,
		required: [true, "USER ID required"]
	},
	productId: {
		type: String,
		required: [true, "PRODUCT ID required"]
	},
	totalAmount: {
		type: Number,
		required: [true, "Total Amount is required"]
	},
	stockOnHand: {
		type: Number,
		required: [true, "Stock on Hand is required"]
	},
	createdOn: {
		type: Date,
		default: new Date()
	}
})


module.exports = mongoose.model("Orders", ordersSchema);