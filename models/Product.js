const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true, "Name is required"],
		unique: true
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},
	price: {
		type: Number,
		required: [true, "Price is required"]
	},
	stock: {
		type: Number,
		required: [true, "Stock required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	orders: [
		{
			userId: {
				type: String,
				required: [true, "User ID is required"]
			},

			totalAmount: {
				type: Number,
				required: [true, "Amount is required"]
			},

			purchasedOn: {
				type: Date,
				default: new Date()
			}
		}
	]

})

module.exports = mongoose.model("Product", productSchema);