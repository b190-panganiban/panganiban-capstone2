const mongoose = require("mongoose");

let userSchema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, "Email is required"],
		unique: true
	},
	password: {
		type: String,
		required: [true, "Password is required"]
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	orderProduct: [
		{
			productId: {
				type: String,
				required: [true, "Product ID is required"]
			},
			orderedOn: {
				type: Date,
				default: new Date()
			}
	}]
})


module.exports = mongoose.model("User", userSchema);