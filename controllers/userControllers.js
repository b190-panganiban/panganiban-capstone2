// import bcrypt module
// Note : bcrypt is for adding security
const bcrypt = require("bcrypt");

// import User model
const User = require('../models/User');

// import Product model
const Product = require('../models/Product');

// import Order model
const Order = require('../models/Orders');

// import auth module
const auth = require("../auth");



module.exports.registerUser = (req, res) => {
	console.log(req.body);


	const hashedPW = bcrypt.hashSync(req.body.password, 10);

	let newUser = new User({
		email: req.body.email,
		password: hashedPW
	})
	

	newUser.save()
	.then(newUser => res.send(newUser))
	.catch(err => {console.log("Email already registered!"); res.send("Email Already registered") });

}


module.exports.loginUser = (req, res) => {
	console.log(req.body);

	User.findOne({email: req.body.email})
	.then(foundUser => {
		if (foundUser === null) {
			return res.send("User does not exist");
		}
		else{

			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)

			if(isPasswordCorrect){
				return res.send({accessToken: auth.createAccessToken(foundUser)})

			}
			else{
				return res.send("Password is incorrect");
			}
		}
	})
};


// Update Regular User to Admin
module.exports.updateUserToAdmin = (req, res) => {
	console.log("ID of ADMIN: " + req.user.id);
	console.log("ID of USER: " + req.params.id);

	let update = {
		isAdmin: true
	}

	User.findByIdAndUpdate(req.params.id, update, {new: true})
	.then(adminToBe => res.send(adminToBe))
	.catch(err => res.send(err));
}


// Order product
module.exports.orderProduct = async (req, res) => {
	// Login User's ID
	console.log(req.user.id);
	
	// if login user Admin execute "Action Forbidden"
	if(req.user.isAdmin){
		return res.send("User is an Admin");
	}

	let isUserUpdated = await User.findById(req.user.id)
	.then(user => {

			console.log(user);

			// add the courseId in an object and push that object into user's enrollment array
			let newOrder = {
				productId: req.body.productId
			};

			console.log(newOrder);

			// The push() method adds new items to the end of an array
			user.orderProduct.push(newOrder);
			
			return user.save().then(user => true).catch(err => err.message)
	})


	// if isUserUpdated does not contain the boolean value of true, we will stop our process and return res.send() to our client with message
	if(isUserUpdated !== true){
		return res.send({message: isUserUpdated})
	}


	let isProductUpdated = await Product.findById(req.body.productId).then(product => {

		console.log(product);
		console.log("STOCKS:" + product.stock);

		// create an object 
		let orderProduct = {
			userId: req.user.id,
			totalAmount: req.body.totalAmount
		};

		product.orders.push(orderProduct);


		// Operation for (-)
		if(product.stock >= req.body.totalAmount && product.stock != 0){
			
			if(req.body.totalAmount === 0 || req.body.totalAmount < 0){
				return "Invalid quantity";
			}

			else{
				let soh = {
					stock: product.stock - req.body.totalAmount
				}

				
				// User.findByIdAndUpdate(req.params.id, update, {new: true})
				// 	.then(adminToBe => res.send(adminToBe))
				// 	.catch(err => res.send(err));			

				Product.findByIdAndUpdate(req.body.productId, soh, {new: true})
				.then(result => console.log("Order Succesful!"))
				.catch(err => res.send(err))


				// Create a record for all orders
				let orderRecords = new Order({
					userId: req.user.id,
					productId: req.body.productId,
					totalAmount: req.body.totalAmount,
					stockOnHand: product.stock - req.body.totalAmount
				});

				orderRecords.save();
			}
		}
		else{
			return "Lack of Stocks";
		}



		return product.save().then(product => true).catch(err => err.message)
	})

	if(isProductUpdated !== true){
		return res.send({message: isProductUpdated})
	}


	if (isUserUpdated && isProductUpdated) {
		
		


		return res.send({message: 'Order Succesfully!'})
	}

}


// Getting all user's order
module.exports.getAllUserOrder = (req, res) => {
	console.log(req.user);
	console.log(req.user.id)

	if(req.user.isAdmin){
		return res.send("User is an Admin");
	}

	Order.find({userId: req.user.id})
	.then(result =>  res.send(result))
	.catch(err => res.send(err));

}