// import product model
const Product = require("../models/Product");


// Adding new Product
module.exports.addNewProduct = (req, res) => {
	console.log(req.body);

	let newProduct = new Product({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		stock: req.body.stock
	});

	newProduct.save()
	.then(product => res.send(product))
	.catch(err => res.send(err));


};


// Retrieve All Products
module.exports.getAllProducts = (req, res) => {
	console.log(req.body);

	Product.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err))
}


// Retrieve Single Products
module.exports.getSingleProduct = (req, res) => {
	console.log(req.params);

	Product.findOne({id: req.params.id})
	.then(result => res.send(result))
	.catch(err => res.send(err))
}


// Update Product Details
module.exports.updateProductDetails = (req, res) => {
	console.log(req.params)
	console.log(req.body);

	let updateDetails = {
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
		stock: req.body.stock
	}

	Product.findByIdAndUpdate(req.params.id, req.body, {new: true})
	.then(result => res.send(result))
	.catch(err => res.send(err))
}


// Archive a product
module.exports.archive = (req, res) => {
	console.log(req.params);

	let archive = {
		isActive: false
	}

	Product.findByIdAndUpdate(req.params.id, archive, {new:true})
	.then(result => res.send(result))
	.catch(err => res.send(err))
}

module.exports.active = (req, res) => {
	console.log(req.params);

	let active = {
		isActive: true
	}

	Product.findByIdAndUpdate(req.params.id, active, {new:true})
	.then(result => res.send(result))
	.catch(err => res.send(err))
}



// Get all Orders by user
module.exports.getAllOrders = (req, res) => {
	console.log(req.user.id);

	Product.findById(req.params.id)
	.then(result => {res.send(result.orders)})
	.catch(err => res.send(err));
}

// Get all active products
module.exports.getActiveProducts = (req, res) => {
	Product.find({isActive: true})
	.then(result => res.send(result))
	.catch(err => res.send(err))
}

// Get all archive products
module.exports.getArchiveProducts = (req, res) => {
	Product.find({isActive: false})
	.then(result => res.send(result))
	.catch(err => res.send(err))
}