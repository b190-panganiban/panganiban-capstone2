const express = require("express");
const mongoose = require("mongoose");


// import User routes
const userRoutes = require("./routes/userRoutes");

// import Product routes
const productRoutes = require("./routes/productRoutes");

// import Orders routes
const ordersRoutes = require("./routes/ordersRoutes");


const app = express();
const port = 4000;


// create a connection with mongoDB
mongoose.connect("mongodb+srv://chrryl:admin@wdc028-ecommerce.dypb93b.mongodb.net/Ecommerce?retryWrites=true&w=majority", 
    {
		useNewUrlParser: true,
		useUnifiedTopology: true
    }
);


// create a variable for mongodb connection using mongoose independency
let db = mongoose.connection;


// Notification if connection to db is successful or not
// Catching any error that may occur from the database connection
db.on("error", console.error.bind(console, "connection error"));


// Once successful a message will log a message to inform that connection is has connected to the server
db.once("open", () => console.log("We're connected to the database"));


// middleware
app.use(express.json());


// routes under '/users'
app.use("/users", userRoutes);

// routes under '/products'
app.use("/products", productRoutes);

// routes under '/orders'
app.use("/orders", ordersRoutes);


// Message if the server is successfully running
app.listen(port, () => console.log(`Server is running at port ${port}`));